package gomqtt

import (
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

// 跟MQTT服务器连接的实例
var client mqtt.Client

// Init 初始化监听器
func Init(opts *mqtt.ClientOptions) (err error) {
	// 设置自定义的连接后处理方法
	opts.SetOnConnectHandler(onConnectHandler(opts.OnConnect))
	// 获取客户端
	client, err = GetClient(opts)
	return
}
