package gomqtt

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"strconv"
	"strings"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

// GetClientOptions 获取MQTT连接配置项
func GetClientOptions(conf *Config) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.SetAutoReconnect(true)

	// 设置Broker 格式：tcp://127.0.0.1:1883或ssl://127.0.0.1:8883
	opts.AddBroker(conf.Broker)
	// 设置用户名
	if username := conf.Username; len(username) > 0 {
		opts.SetUsername(username)
	}
	//设置密码
	if password := conf.Password; len(password) > 0 {
		opts.SetPassword(password)
	}
	// 设置ClientID
	if clientId := conf.ClientID; len(clientId) > 0 {
		opts.SetClientID(clientId)
	} else {
		opts.SetClientID(strconv.FormatInt(time.Now().UnixNano(), 10))
	}

	// 配置证书
	if strings.HasPrefix(conf.Broker, "ssl") {
		// Import trusted certificates from CAfile.pem.
		// Alternatively, manually add CA certificates to
		// default openssl CA bundle.
		certPool := x509.NewCertPool()
		pemCerts, err := ioutil.ReadFile(conf.CACert)
		if err == nil {
			certPool.AppendCertsFromPEM(pemCerts)
		}

		// Import client certificate/key pair
		var certificates []tls.Certificate
		if len(conf.ClientCert) > 0 && len(conf.ClientKey) > 0 {
			cert, err := tls.LoadX509KeyPair(conf.ClientCert, conf.ClientKey)
			if err != nil {
				panic(err)
			}
			certificates = []tls.Certificate{cert}
		}

		opts.SetTLSConfig(&tls.Config{
			// RootCAs = certs used to verify server cert.
			RootCAs: certPool,
			// ClientAuth = whether to request cert from server.
			// Since the server is set up for SSL, this happens
			// anyways.
			ClientAuth: tls.NoClientCert,
			// ClientCAs = certs used to validate client cert.
			ClientCAs: nil,
			// InsecureSkipVerify = verify that cert contents
			// match server. IP matches what is in cert etc.
			InsecureSkipVerify: true,
			// Certificates = list of certs client sends to server.
			Certificates: certificates,
		})
	}
	return opts
}
