package gomqtt

import (
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type SubscribeType struct {
	Topic      string
	Qos        byte
	Callback   mqtt.MessageHandler
	RetryTimes int // 为0表示无限重试
}

var subscribers = make([]SubscribeType, 0)

// Subscribe 注册订阅消息
func Subscribe(item SubscribeType) {
	subscribers = append(subscribers, item)
}

func subscribe(item SubscribeType) {
	times := 0
	for {
		token, err := subscribeItem(item)
		if err != nil {
			if item.RetryTimes == 0 || times < item.RetryTimes {
				times++
				time.Sleep(3 * time.Second)
				continue
			} else {
				panic(err)
			}
		}
		if token.Wait() && token.Error() != nil {
			if item.RetryTimes == 0 || times < item.RetryTimes {
				times++
				time.Sleep(3 * time.Second)
				continue
			} else {
				panic(token.Error())
			}
		}
		break
	}
}

func subscribeItem(item SubscribeType) (token mqtt.Token, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = e.(error)
		}
	}()
	token = client.Subscribe(item.Topic, item.Qos, item.Callback)
	return
}
