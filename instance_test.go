/*
 * @Description  :
 * @Date         : 2020-04-16 11:35:43
 * @LastEditors  : Hu
 * @LastEditTime : 2021-07-04 11:42:21
 * @FilePath     : \gomqtt\instance_test.go
 */
package gomqtt

import (
	"fmt"
	"testing"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func TestTCP(t *testing.T) {
	opts := GetClientOptions(&Config{
		Broker:   "tcp://192.168.31.100:1883",
		Username: "admin",
		Password: "123456",
		ClientID: "1234567890",
	})
	opts.SetOnConnectHandler(func(c mqtt.Client) {
		fmt.Println("TCP OnConnect.")
	})
	if err := Init(opts); err != nil {
		fmt.Println(err)
	}

	time.Sleep(time.Second * 5)
}

func TestSSL(t *testing.T) {
	opts := GetClientOptions(&Config{
		Broker:     "ssl://192.168.31.100:8883",
		Username:   "admin",
		Password:   "123456",
		CACert:     "E:/certs/cacert.pem",
		ClientCert: "E:/certs/client-cert.pem",
		ClientKey:  "E:/certs/client-key.pem",
		ClientID:   "1234567890",
	})
	opts.SetOnConnectHandler(func(c mqtt.Client) {
		fmt.Println("SSL OnConnect.")
	})
	if err := Init(opts); err != nil {
		fmt.Println(err)
	}

	time.Sleep(time.Second * 5)
}
