package gomqtt

import mqtt "github.com/eclipse/paho.mqtt.golang"

// GetClient 获取MQTT连接
func GetClient(opts *mqtt.ClientOptions) (client mqtt.Client, err error) {
	opts.SetAutoReconnect(true)
	opts.SetCleanSession(true)
	client = mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		err = token.Error()
	}
	return
}
