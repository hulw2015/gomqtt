package gomqtt

// Publish 通用发布消息接口
func Publish(topic string, payload interface{}, qos byte, retained bool) (err error) {
	token := client.Publish(topic, qos, retained, payload)
	if token.Wait() && token.Error() != nil {
		err = token.Error()
	}
	return
}
