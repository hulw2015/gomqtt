package gomqtt

// Config MQTT的配置信息格式
type Config struct {
	Broker     string // Broker地址，例如tcp://127.0.0.1:1883或ssl://127.0.0.1:8883. 如果配置ssl，则必须配置CACert
	Username   string // 用户名，可选
	Password   string // 密码，可选
	CACert     string // CA证书，单向认证只需要配置此文件即可，无需ClientCert和ClientKey，可选
	ClientCert string // ClientCert，可选
	ClientKey  string // ClientKey，可选
	ClientID   string // ClientID，可选
}
