package gomqtt

import mqtt "github.com/eclipse/paho.mqtt.golang"

// 连接上服务器的操作
func onConnectHandler(handler mqtt.OnConnectHandler) mqtt.OnConnectHandler {
	return func(c mqtt.Client) {
		for _, item := range subscribers {
			subscribe(item)
		}
		if handler != nil {
			handler(c)
		}
	}
}
